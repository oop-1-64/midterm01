/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.midterm01;

/**
 *
 * @author domem
 */
public class Tree {

    private String name;
    private int heightCen;
    private int annualRing;

    public Tree(String name, int heightCen, int annualRing) {
        this.name = name;
        this.heightCen = heightCen;
        this.annualRing = annualRing;
    }

    public boolean checkType() {
        if (heightCen >= 240 && annualRing > 0) {                               //Check height >= 240 and Check count of annualRing >= 0.
            System.out.println("Yes! " + this.name + " is perennial.");
            checkAge(); //if true then call checkAge method.
            return true;
        }
        System.out.println("No! " + this.name + " isn't perennial.");
        newline();                                                              //if false then call newline and out
        return false;

    }

    public void checkAge() {                                                    //Check Age of tree by look count of Annual Ring
        System.out.println("AnnualRing is " + annualRing + " years old.");
        newline();
    }

    public void newline() {
        System.out.println("==================================");
    }

}
